module Main exposing (main)

import Browser
import FeatherIcons
import Html
import Html.Attributes
import Html.Events exposing (onClick, onInput)


main =
    Browser.sandbox { init = init, update = update, view = view }


type alias Model =
    { input : String
    , scroll : Int
    , pageHeight : Int
    , scrollStep : Int
    }


init : Model
init =
    { input = ""
    , scroll = 0
    , pageHeight = 10
    , scrollStep = 2
    }


type Msg
    = InputChanged String
    | UpButtonClicked
    | DownButtonClicked


update : Msg -> Model -> Model
update msg model =
    case msg of
        InputChanged newValue ->
            { model | input = newValue }

        UpButtonClicked ->
            { model
                | scroll =
                    model.scroll
                        - model.scrollStep
                        |> max 0
            }

        DownButtonClicked ->
            let
                scrollLimit : Int
                scrollLimit =
                    linesCount - model.pageHeight

                linesCount : Int
                linesCount =
                    model.input
                        |> String.lines
                        |> List.length
            in
            { model
                | scroll =
                    model.scroll
                        + model.scrollStep
                        |> min scrollLimit
            }


view : Model -> Html.Html Msg
view model =
    Html.div []
        [ Html.textarea
            [ Html.Attributes.value model.input
            , onInput InputChanged
            , Html.Attributes.placeholder "Put text pleaseee"
            , Html.Attributes.cols 80
            , Html.Attributes.rows 10
            ]
            []
        , Html.pre []
            [ model.input
                |> page model.pageHeight model.scroll
                |> Html.text
            ]
        , Html.button [ onClick UpButtonClicked ]
            [ FeatherIcons.arrowUp
                |> FeatherIcons.toHtml []
            ]
        , Html.button [ onClick DownButtonClicked ]
            [ FeatherIcons.arrowDown
                |> FeatherIcons.toHtml []
            ]
        ]


page : Int -> Int -> String -> String
page pageHeight scroll input =
    input
        |> wrapParagraphs 80
        |> String.lines
        |> List.drop scroll
        |> List.take pageHeight
        |> String.join "\n"


wrapParagraphs : Int -> String -> String
wrapParagraphs length input =
    input
        |> paragraphs
        |> List.map (wrap length)
        |> String.join "\n\n"


paragraphs : String -> List String
paragraphs input =
    String.split "\n\n" input


wrap : Int -> String -> String
wrap length paragraph =
    paragraph
        |> String.split " "
        |> groupLines length
        |> String.join "\n"


groupLines : Int -> List String -> List String
groupLines length words =
    List.foldl
        (\word memo ->
            let
                fits : Bool
                fits =
                    String.length word
                        + String.length line
                        + 1
                        < length

                line : String
                line =
                    memo
                        |> List.head
                        |> Maybe.withDefault ""

                previousLines : List String
                previousLines =
                    memo
                        |> List.tail
                        |> Maybe.withDefault []
            in
            if fits then
                (line ++ " " ++ word)
                    :: previousLines

            else
                word :: line :: previousLines
        )
        []
        words
        |> List.reverse
