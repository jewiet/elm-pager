.PHONY: develop
develop:
	npx parcel src/index.pug

.PHONY: clean
clean:
	rm -rf node_modules dist .cache elm-stuff
